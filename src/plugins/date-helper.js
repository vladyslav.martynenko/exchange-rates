export function getISODate(date = Date.now()) {
  return new Date(date).toISOString().split('T')[0];
}

export default getISODate;
